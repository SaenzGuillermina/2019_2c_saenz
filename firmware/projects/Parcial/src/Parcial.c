/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Parcial.h"

#include "systemclock.h"
#include "gpio.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"
#include "switch.h"
#include "DisplayITS_E0803.h"




/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/



//Frecuencia de muestreo
#define SAMPLE_FREC_US (1000000/100)

//Respuesta en frecuencia
#define LPF_BETA_1 0.25
#define LPF_BETA_2 0.55
#define LPF_BETA_3 0.75

//Variable que contiene el dato
uint16_t dato_senial;

//Variable que contiene el valor ya filtrado
uint16_t valor_filtrado;

//Variable que contiene el valor anterior para el filtro
uint16_t valor_filtrado_anterior=0;

//Variable de respuesta en frecuencia
uint16_t lpf_beta= LPF_BETA_1 ;



/*==================[internal functions declaration]=========================*/

/** @brief Función llamada por timer.
 * Activa conversión AD.
 */
void TempInt();

/** @brief Función llamada por interrupción conversor AD.
 * Lee dato convertido
 */
void LecturaAD();

/** @brief Función llamada por interrupción de tecla 2.
 * Estabnlece respuesta en frecuencia de 0.25
 */
void Tecla2();

/** @brief Función llamada por interrupción de tecla 3.
 * Estabnlece respuesta en frecuencia de 0.55
 */
void Tecla3();

/** @brief Función llamada por interrupción de tecla 4.
 * Estabnlece respuesta en frecuencia de 0.75
 */
void Tecla4();



/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	SystemClockInit();
	SwitchesInit();

	//Inicializa el display
	gpio_t pines[7] = {LCD1, LCD2, LCD3, LCD4, GPIO1, GPIO3, GPIO5};
	ITSE0803Init(pines);


	//Fijamos configuración para inicializar conversor AD
	analog_input_config ad_config;
	ad_config.input = CH1;
	ad_config.mode = AINPUTS_SINGLE_READ;
	ad_config.pAnalogInput = LecturaAD;

	AnalogInputInit(&ad_config);
	AnalogOutputInit();

	//Estructura para inicializar timer
	timer_config temporizador;
	temporizador.period = SAMPLE_FREC_US; //Período de muestreo
	temporizador.timer = TIMER_A; //Se usa SysTick
	temporizador.pFunc = TempInt;
	TimerInit(&temporizador);

	TimerStart(TIMER_A);

	//Estructura para inicializar uart
	serial_config puerto_serie;
	puerto_serie.port = SERIAL_PORT_PC;	//Usamos puerto PC
	puerto_serie.baud_rate = 115200; //bits por segundo (no especifica el problema)
	puerto_serie.pSerial = NO_INT; //No usamos una función porque sólo enviamos datos

	UartInit(&puerto_serie);

	//Interrupciones de teclas
	SwitchActivInt(SWITCH_2, Tecla2);
	SwitchActivInt(SWITCH_3, Tecla3);
	SwitchActivInt(SWITCH_4, Tecla4);

	//Variable para casteo
	uint8_t casteo;


	while(1)
	{

		valor_filtrado =  valor_filtrado_anterior - (lpf_beta * (valor_filtrado_anterior - dato_senial));
		valor_filtrado_anterior=valor_filtrado;
		casteo=lpf_beta;
		ITSE0803DisplayValue(casteo);
		UartSendString(puerto_serie.port, UartItoa(dato_senial,10));
		UartSendString(puerto_serie.port, ",");
		UartSendString(puerto_serie.port, UartItoa(valor_filtrado,10));
		UartSendString(puerto_serie.port, "\r\n");



	}
	return 0;
}


//Función del temporizador
void TempInt()
{
	//Función que comienza a convertir el dato AD, una vez que termina de convertir, llama a la función LecturaAD()
	AnalogStartConvertion();
}



void LecturaAD()
{
	//Lee dato y lo guarda en variable dato_senial
	AnalogInputRead(CH1, &dato_senial);
	AnalogOutputWrite(dato_senial);

}


void Tecla2()
{
	lpf_beta= LPF_BETA_1 ;
}

void Tecla3()
{
	lpf_beta= LPF_BETA_2 ;
}

void Tecla4()
{
	lpf_beta= LPF_BETA_3 ;
}



/*==================[end of file]============================================*/


