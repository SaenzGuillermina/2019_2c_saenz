﻿Parcial

Se implementa firmware de un sistema de adquisición, este procesando la señal de un acelerómetro, dicho procesamiento se realiza a 100Hz. El procesamiento realizado es: 
valor_filtrado[n] =  valor_filtrado[n-1] - (lpf_beta * (valor_filtrado[n-1] - valor_crudo[n])). Los valores procesados y anteriores al procesamiento son enviados a la PC por
puerto serie. Ademas se mostrará en un display la resolución en frecuencia empleada.

Para implementar la aplicación, se requiere la conexión de:

	Un acelerómetro
	Un display


El acelerómetro se conecta:
	GND a GNDA
	OUT a CH1 (P1,13)
	
El display se conecta a los pines LCD1, LCD2, LCD3, LCD4, GPIO1, GPIO3, GPIO5


