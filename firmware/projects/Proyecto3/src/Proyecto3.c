/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
//#include "../inc/Proyecto2_Ultrasonido.h"
//#include "../inc/Proyecto2_Ultrasonido.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "gpio.h"
#include "timer.h"
#include "switch.h"
#include "hc_sr4.h"
#include "DisplayITS_E0803.h"
#include "uart.h"

/*==================[macros and definitions]=================================*/

#define NO_KEY 0

/*==================[internal data definition]===============================*/
int16_t distancia=0;
bool in_cm = false; // 0  si en cm y 1 si en in
bool on_off = true;
bool display = false;
bool timer_on_off=true;

/*==================[internal functions declaration]=========================*/
void Tecla1(); //Función de interrupción tecla 1, pausa la lectura
void Tecla2(); //Función de interrupción tecla 2, retiene la lectura
void Tecla3(); //Función de interrupción tecla 3, lee en pulgadas
void Tecla4(); //Función de interrupción tecla 4, lee en centimetros

void ActivacionDeLectura();
/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	serial_config puerto_serie;
	puerto_serie.port = SERIAL_PORT_PC;  // configuramos que puerto queremos usar
	puerto_serie.baud_rate = 115200; // cantidad de datos por segundo
	puerto_serie.pSerial = NO_INT;  // configuramos para no recibir datos
	UartInit(&puerto_serie);
	HcSr04Init(T_FIL2,T_FIL3);


	gpio_t pines[7] = {LCD1, LCD2, LCD3, LCD4, GPIO1, GPIO3, GPIO5};
	ITSE0803Init(pines);

	timer_config temporizador;
	temporizador.period=1000;
	temporizador.timer=TIMER_B;
	temporizador.pFunc=ActivacionDeLectura;

	TimerInit(&temporizador);
	TimerStart(TIMER_B);

	SwitchActivInt(SWITCH_1, Tecla1);
	SwitchActivInt(SWITCH_2, Tecla2);
	SwitchActivInt(SWITCH_3, Tecla3);
	SwitchActivInt(SWITCH_4, Tecla4);

    while(1)
    {

    	if(on_off == true) //si la lectura esta activada
    	{
    		if(in_cm == false) // muestro la lectura en cm
    		{
    			distancia = HcSr04ReadDistanceCentimeters();
    			UartSendString(puerto_serie.port,UartItoa(distancia, 10)); // como UartItoa devuelve un string lo uso directamente
    			UartSendString(puerto_serie.port, " cm\r\n");
    		}

    		if(in_cm == true) // muestro la lectura en pulgadas
    		{
    			distancia = HcSr04ReadDistanceInches();
    			UartSendString(puerto_serie.port,UartItoa(distancia, 10));
    			UartSendString(puerto_serie.port, " in\r\n");
    		}

        	on_off=false;
    	}


    	if(display == false) // actualiza lo que muestra el display
    		ITSE0803DisplayValue(distancia);



	}
    
	return 0;
}

void Tecla1()
{
	if(timer_on_off==true)
		TimerStop(TIMER_B);
	if(timer_on_off==false)
		TimerStart(TIMER_B);
	timer_on_off=!timer_on_off;
}
void Tecla2(){
	display = !display;
}
void Tecla3(){
	in_cm = false;
}
void Tecla4(){
	in_cm = true;
}

void ActivacionDeLectura(){
	on_off= true;
}

/*==================[end of file]============================================*/

