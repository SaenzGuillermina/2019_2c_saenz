########################################################################
#Cátedra: Electrónica Programable
#FIUNER
#2018
#Autor/es:
#JMreta - jmreta@ingenieria.uner.edu.ar
#
#Revisión:
########################################################################

# Ruta del Proyecto
####Ejemplo 1: Hola Mundo
##Por el primero, se debe usar Makefile1
#PROYECTO_ACTIVO = 1_hola_mundo
#NOMBRE_EJECUTABLE = hola_mundo.exe

####Ejemplo 2: Control Mundo
#PROYECTO_ACTIVO = 2_control_mundo
#NOMBRE_EJECUTABLE = control_mundo.exe

####Ejemplo 3: Promedio
#PROYECTO_ACTIVO = 3_promedio_1
#NOMBRE_EJECUTABLE = promedio.exe

#PROYECTO_ACTIVO = 5_menu
#NOMBRE_EJECUTABLE = menu.exe

#PROYECTO_ACTIVO = Ej_c_d_e
#NOMBRE_EJECUTABLE = c_d_e.exe

#PROYECTO_ACTIVO = Guia1_ejer1
#NOMBRE_EJECUTABLE = Guia1_ejer1.exe

#PROYECTO_ACTIVO = Guia1_ejer2
#NOMBRE_EJECUTABLE = Guia1_ejer2.exe

#PROYECTO_ACTIVO = Guia1_ejer7
#NOMBRE_EJECUTABLE = Guia1_ejer7.exe

#PROYECTO_ACTIVO = Guia1_ejer
#NOMBRE_EJECUTABLE = Guia1_ejer9.exe

#PROYECTO_ACTIVO = Guia1_ejer12
#NOMBRE_EJECUTABLE = Guia1_ejer12.exe

#PROYECTO_ACTIVO = Guia1_ejer14
#NOMBRE_EJECUTABLE = Guia1_ejer14.exe

#PROYECTO_ACTIVO = Guia1_ejer16
#NOMBRE_EJECUTABLE = Guia1_ejer16.exe

#PROYECTO_ACTIVO = Guia1_EjercicioIntegradorA
#NOMBRE_EJECUTABLE = Guia1_EjercicioIntegradorA.exe

#PROYECTO_ACTIVO = Guia1_EjercicioIntegradorB
#NOMBRE_EJECUTABLE = Guia1_EjercicioIntegradorB.exe

#PROYECTO_ACTIVO = Guia1_EjercicioIntegradorC
#NOMBRE_EJECUTABLE = Guia1_EjercicioIntegradorC.exe

PROYECTO_ACTIVO = Guia1_EjercicioIntegradorD
NOMBRE_EJECUTABLE = Guia1_EjercicioIntegradorD.exe