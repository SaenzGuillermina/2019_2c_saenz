/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdint.h>
#include <stdio.h>
#include <string.h>

/*==================[macros and definitions]=================================*/
typedef struct {
    uint8_t n_led;        //indica el número de led a controlar
    uint8_t n_ciclos;   //indica la cantidad de cilcos de encendido/apagado
    uint8_t periodo;    //indica el tiempo de cada ciclo
    uint8_t mode;       //ON, OFF, TOGGLE
}my_leds;

#define ON 1
#define OFF 0
#define TOGGLE 2
uint8_t n,j;

void ManejarLeds (my_leds *p){
	switch(p->mode){
	case ON:
		switch (p->n_led){
		case 1:
			printf("prende led 1\n\r");
			break;
		case 2:
			printf("prende led 2\n\r");
			break;
		case 3:
			printf("prende led 3\n\r");
			break;
		}
		break;

	case OFF:
		switch(p->n_led){
		case 1:
			printf("apaga led 1\n\r");
			break;
		case 2:
			printf("apaga led 2\n\r");
			break;
		case 3:
			printf("apaga led 3\n\r");
			break;
		}
		break;
	case TOGGLE:
		for(n=0; n<p->n_ciclos; n++){
			switch (p->n_led){
			case 1:
				printf("toggle led 1\n\r");
				break;
			case 2:
				printf("toggle led 2\n\r");
				break;
			case 3:
				printf("toggle led 3\n\r");
				break;

			}

			for(j=0; j<p->periodo; j++){
				printf("retardo\n\r");
			}

		}
	    break;
	}
}

/*==================[internal functions declaration]=========================*/

int main(void)
{
 my_leds led={
		 1,5,10,TOGGLE
 };
 ManejarLeds(&led);
	return 0;
}

/*==================[end of file]============================================*/

