/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
/*Declare una variable sin signo de 32 bits y cargue el valor 0x01020304.
 Declare cuatro variables sin signo de 8 bits y, utilizando máscaras, rotaciones y truncamiento,
 cargue cada uno de los bytes de la variable de 32 bits.
Realice el mismo ejercicio, utilizando la definición de una “union”.

 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdint.h>
#include <stdio.h>
#include <string.h>


/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/

int main(void)
{ /*uint32_t a=0x01020304;
  printf("%d\r\n", a);
  uint8_t b,c,d,e;
  b=a;
  printf("%d\r\n", b);
  c=(a>>8);
  printf("%d\r\n", c);
  d=(a>>16);
  printf("%d\r\n", d);
  e=(a>>24);
  printf("%d\r\n", e);*/

 typedef struct {
	 uint8_t b;
	 uint8_t c;
	 uint8_t d;
	 uint8_t e;

 } A;

 union {
	 A g;
	 uint32_t a;
 } B;
 B.a=0x01020304;
 printf("%d\r\n", B.g.c);

	return 0;
}

/*==================[end of file]============================================*/

